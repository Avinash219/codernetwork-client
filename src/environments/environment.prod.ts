export const environment = {
  production: true,
  devUrl: 'http://localhost:8000/',
  baseUrl: 'api/',
};
