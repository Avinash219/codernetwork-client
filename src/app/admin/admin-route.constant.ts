export const AdminRouteConstant = {
  GET_ALL_REF_DATA: 'refData/getTagList',
  ADD_REF_DATA: 'refData/addTag',
  ADD_REF_DATA_VALUE: 'refData/addRefDataValue',
};
