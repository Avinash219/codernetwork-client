import { UserService } from './../user.service';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  selector: 'app-reply-form',
  templateUrl: './reply-form.component.html',
  styleUrls: ['./reply-form.component.scss'],
})
export class ReplyFormComponent implements OnInit {
  @ViewChild('message') message: ElementRef;
  @Input('postId') postId: number;
  constructor(private _useService: UserService) {}

  ngOnInit(): void {}

  addComment() {
    this._useService
      .saveAnswer({
        answer: this.message.nativeElement.value,
        answerOf: this.postId,
      })
      .subscribe((response) => {
        console.log(response);
      });
  }
}
