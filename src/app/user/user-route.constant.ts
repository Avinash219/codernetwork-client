export const UserRoutingConstant = {
  GET_USER_INFO: 'profile/user/',
  UPDATE_USER_INFO: 'profile/user/update/',
  ADD_TIP: 'tip/addTip',
  GET_TIP_LIST: 'tip/getTip',
  ADD_QUESTION: 'questionAnswer/addQuestion',
  GET_QUESTION_LIST: 'questionAnswer/getQuestion',
  SAVE_ANSWER: 'questionAnswer/submitAnswer',
  GET_TAG_LIST: 'refData/getTagList',
  GET_QUESTIONS_ANSWER: 'questionAnswer/questionsAnswer',
};
