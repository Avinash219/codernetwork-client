import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { startWith, map } from 'rxjs/operators';
import { UserService } from '../user.service';

@Component({
  selector: 'app-tips',
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.scss'],
})
export class TipsComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  learningForm: FormGroup;
  tagList: any = ['abc', 'def'];
  filteredTags: any;
  addedTags: string[] = [];
  addedTagsId: any[] = [];
  constructor(private _fb: FormBuilder, private _userService: UserService) {}

  ngOnInit(): void {
    this.initForm();
    this._userService.getTagList().subscribe((response) => {
      this.tagList = response['data'];
      this.filteredTags = this.learningForm.get('postTag').valueChanges.pipe(
        startWith(null),
        map((tag) => (tag ? this._filter(tag) : this.tagList.slice()))
      );
    });
  }

  initForm() {
    this.learningForm = this._fb.group({
      learningPost: [''],
      postTag: [''],
    });
  }

  add(event: any): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.addedTags.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.learningForm.get('postTag').setValue(null);
  }

  remove(tag: string): void {
    const index = this.addedTags.indexOf(tag);

    if (index >= 0) {
      this.addedTags.splice(index, 1);
      this.tagList.push(tag);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.addedTags.push(event.option.viewValue);
    this.addedTagsId.push(event.option.value);
    this.learningForm.get('postTag').setValue(null);
  }

  private _filter(value: string): void {
    this.tagList = this.tagList.filter((tag) => {
      return tag !== value;
    });
  }

  submitPost() {
    this._userService
      .saveTip({
        tip: this.learningForm.get('learningPost').value,
        tagList: this.addedTagsId,
      })
      .subscribe((response) => {
        console.log(response);
      });
  }
}
