import { UserRoutingConstant } from './user-route.constant';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private _http: HttpClient) {}

  getUserDetail(username) {
    return this._http.get(`${UserRoutingConstant.GET_USER_INFO}${username}`);
  }

  updateUserDetail(formData) {
    return this._http.post(`${UserRoutingConstant.UPDATE_USER_INFO}`, formData);
  }

  saveTip(body) {
    return this._http.post(`${UserRoutingConstant.ADD_TIP}`, body);
  }

  getTipList() {
    return this._http.get(`${UserRoutingConstant.GET_TIP_LIST}`);
  }

  saveQuestion(body) {
    return this._http.post(`${UserRoutingConstant.ADD_QUESTION}`, body);
  }

  getQuestionList() {
    return this._http.get(`${UserRoutingConstant.GET_QUESTION_LIST}`);
  }

  saveAnswer(body) {
    return this._http.post(`${UserRoutingConstant.SAVE_ANSWER}`, body);
  }

  getTagList() {
    return this._http.get(`${UserRoutingConstant.GET_TAG_LIST}`);
  }

  getQuestionsAnswer(questionId) {
    return this._http.get(
      `${UserRoutingConstant.GET_QUESTIONS_ANSWER}/${questionId}`
    );
  }
}
